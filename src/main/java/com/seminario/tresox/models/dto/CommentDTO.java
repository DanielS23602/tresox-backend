package com.seminario.tresox.models.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CommentDTO {
    private int comm_content;
    private String comm_user;
    private String comm_comment;
}
