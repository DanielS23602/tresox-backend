package com.seminario.tresox.models.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ContentDTO {
    private Long cont_id;
    private int cont_stock;
}
