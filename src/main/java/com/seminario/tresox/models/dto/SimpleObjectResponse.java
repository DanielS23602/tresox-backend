package com.seminario.tresox.models.dto;

import lombok.*;

@AllArgsConstructor
@Builder
@Getter
public class SimpleObjectResponse {

    private int code;
    private String message;
    private Object value;


}
