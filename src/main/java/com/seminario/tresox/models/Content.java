package com.seminario.tresox.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@Entity
@Table(name = "tb_content")
@ApiModel(value = "Content - Model", description = "Entidad para administrar la información del contenido de las peliculas")

public class Content implements Serializable {
    private  static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="cont_id")
    private Long cont_id;

    @NotBlank(message = "El campo cont_name no debe ser nulo")
    @Column(name="cont_name")
    private String cont_name;

    @Column(name="cont_review")
    private int cont_review;

    @ManyToOne
    @JoinColumn(name = "content_typect_id")
    @JsonManagedReference
    private ContentType cont_type;

    @OneToOne
    @JoinColumn(name = "previewprev_id")
    private Preview preview;

    @Column(name="cont_stock")
    private int stock;

    @OneToMany(mappedBy = "dir_content")
    private List<DirectorContent> directors_content;

    @OneToMany(mappedBy = "img_content")
    private List<ImageContent> images_content;

    @OneToMany(mappedBy = "act_content")
    private List<ActorContent> actors_content;

    @OneToMany(mappedBy = "comm_content")
    private List<Comment> comments_content;

}
