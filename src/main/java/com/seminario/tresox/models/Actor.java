package com.seminario.tresox.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.Getter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;


@Getter
@Entity
@Table(name = "tb_actor")
@ApiModel(value = "Actor - Model", description = "Entidad para administrar la información de los actores")

public class Actor implements Serializable {

    private  static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="act_id")
    private Long act_id;

    @NotBlank(message = "El campo act_name no debe ser nulo")
    @Column(name="act_name")
    private String act_name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "actor", fetch = FetchType.EAGER)
    @JsonIgnore
    private List<ActorContent> actorContents;


}
