package com.seminario.tresox.models;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(name = "tb_user")
@ApiModel(value = "Usuario - Model", description = "Entidad para administrar la información de los usuarios")
public class User implements Serializable {

    private  static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="us_id")
    private Long id;

    @NotBlank(message = "El campo us_name no debe ser nulo")
    @Column(name="us_name")
    private String name;

    @NotBlank(message = "El campo us_email no debe ser nulo")
    @Column(name="us_email")
    private String email;

    @NotBlank(message = "El campo us_password no debe ser nulo")
    @Column(name="us_password")
    private String password;

}
