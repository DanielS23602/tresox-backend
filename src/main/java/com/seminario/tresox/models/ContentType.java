package com.seminario.tresox.models;


import com.fasterxml.jackson.annotation.JsonBackReference;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Getter
@Entity
@Table(name = "tb_content_type")
@ApiModel(value = "ContentType - Model", description = "Entidad para administrar la información del tipo de contenido; series, peliculas, etc...")
public class ContentType {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="ct_id")
    private Long ct_id;

    @NotBlank(message = "El campo ct_name no debe ser nulo")
    @Column(name="ct_name")
    private String ct_name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cont_type", fetch = FetchType.EAGER)
    @JsonBackReference
    @ToString.Exclude
    private List<Content> content;

}
