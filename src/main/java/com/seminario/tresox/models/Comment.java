package com.seminario.tresox.models;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "tb_comment")
@ApiModel(value = "Content - Model", description = "Entidad para administrar la información del contenido de las peliculas")

public class Comment implements Serializable {

    private  static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="comm_id")
    private Long comm_id;

    @NotBlank(message = "El campo comm_user no debe ser nulo")
    @Column(name="comm_user")
    private String comm_user;

    @NotBlank(message = "El campo comm_comment no debe ser nulo")
    @Column(name="comm_comment")
    private String comm_comment;

    @Column(name="comm_fk_cont_id")
    private int comm_content;
}
