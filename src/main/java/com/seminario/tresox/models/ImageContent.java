package com.seminario.tresox.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.Getter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Getter
@Entity
@Table(name = "tb_images_content")
@ApiModel(value = "Image - Model", description = "Entidad para administrar la información del contenido de las images")
public class ImageContent  implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="imgc_id")
    private Long imgc_id;

    @NotBlank(message = "El campo img_name no debe ser nulo")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "imageimg_id")
    private Image image;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "contentcont_id")
    @JsonIgnore
    private Content img_content;
}
