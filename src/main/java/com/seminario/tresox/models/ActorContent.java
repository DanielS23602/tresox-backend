package com.seminario.tresox.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.Getter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Getter
@Entity
@Table(name = "tb_actors_content")
@ApiModel(value = "ActorContent - Model", description = "Entidad para administrar la información del contenido del actor ")
public class ActorContent implements Serializable {
    private  static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="actc_id")
    private Long actc_id;

    @NotBlank(message = "El campo actoract_id no debe ser nulo")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "actoract_id")
    private Actor actor;

    @NotBlank(message = "El campo contentcont_id no debe ser nulo")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "contentcont_id")
    @JsonIgnore
    private Content act_content;

}
