package com.seminario.tresox.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.Getter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Getter
@Entity
@Table(name = "tb_preview")
@ApiModel(value = "Preview - Model", description = "Entidad para administrar la información del contenido del preview")
public class Preview {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="prev_id")
    private Long prev_id;

    @NotBlank(message = "El campo prev_path no debe ser nulo")
    @Column(name="prev_path")
    private String prev_path;

    @OneToOne(mappedBy = "preview", fetch = FetchType.LAZY)
    @JsonIgnore
    private Content prev_content;

}
