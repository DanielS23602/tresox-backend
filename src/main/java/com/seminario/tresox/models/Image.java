package com.seminario.tresox.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.Getter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;


@Getter
@Entity
@Table(name = "tb_image")
@ApiModel(value = "Image - Model", description = "Entidad para administrar la información de las images")
public class Image implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="img_id")
    private Long img_id;

    @NotBlank(message = "El campo img_name no debe ser nulo")
    @Column(name="img_name")
    private String img_name;

    @NotBlank(message = "El campo img_path no debe ser nulo")
    @Column(name="img_path")
    private String img_path;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "image", fetch = FetchType.EAGER)
    @JsonIgnore
    private List<ImageContent> imageContentList;
}
