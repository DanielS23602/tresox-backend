package com.seminario.tresox.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;


@Getter
@Entity
@Table(name = "tb_directors_content")
@ApiModel(value = "DirectorContent - Model", description = "Entidad para administrar la información del contenido del director")
public class DirectorContent implements Serializable {

    private  static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="dirc_id")
    private Long dirc_id;

    @NotBlank(message = "El campo actoract_id no debe ser nulo")
    @ManyToOne
    @JoinColumn(name = "directordir_id")
    private Director director;

    @NotBlank(message = "El campo contentcont_id no debe ser nulo")
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "contentcont_id")
    private Content dir_content;
}
