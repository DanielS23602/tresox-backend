package com.seminario.tresox.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;


@Getter
@Entity
@Table(name = "tb_director")
@ApiModel(value = "Director - Model", description = "Entidad para administrar la información del contenido del director")

public class Director  implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="dir_id")
    private Long dir_id;

    @NotBlank(message = "El campo dir_name no debe ser nulo")
    @Column(name="dir_name")
    private String dir_name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "director", fetch = FetchType.EAGER)
    @JsonIgnore
    private List<DirectorContent> directorContents;

}
