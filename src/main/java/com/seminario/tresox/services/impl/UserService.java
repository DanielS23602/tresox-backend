package com.seminario.tresox.services.impl;

import com.seminario.tresox.models.User;
import com.seminario.tresox.models.dto.UserDTO;
import com.seminario.tresox.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User update(User user) {
        return userRepository.save(user);
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public UserDetails loadUserByUsername(String email) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException("Usuario no encontrado con el correo: " + email);
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), new ArrayList<>());
    }

    public User save(UserDTO user) {
        if (userRepository.findByEmail(user.getEmail()) == null) {
            User newUser = new User();
            newUser.setEmail(user.getEmail());
            try {
                newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
            } catch (Exception e) {
                System.err.println(e);
            }
            newUser.setName(user.getName());
            return userRepository.save(newUser);
        } else {
            return null;
        }
    }

    public Object deleteById(Long id) {
        userRepository.deleteById(id);
        return null;
    }
}
