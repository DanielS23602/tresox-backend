package com.seminario.tresox.services.impl;

import com.seminario.tresox.models.Comment;
import com.seminario.tresox.models.dto.CommentDTO;
import com.seminario.tresox.repositories.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentService {
    private CommentRepository commentRepository;

    @Autowired
    CommentService(CommentRepository commentRepository){
        this.commentRepository = commentRepository;
    }

    public Comment saveComment(CommentDTO commentDTO){
        Comment comment = new Comment();
        comment.setComm_content(commentDTO.getComm_content());
        comment.setComm_user(commentDTO.getComm_user());
        comment.setComm_comment(commentDTO.getComm_comment());
        return commentRepository.save(comment);
    }
}
