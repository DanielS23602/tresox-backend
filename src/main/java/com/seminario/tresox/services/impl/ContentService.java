package com.seminario.tresox.services.impl;

import com.seminario.tresox.models.Content;
import com.seminario.tresox.models.ContentType;
import com.seminario.tresox.models.dto.ContentDTO;
import com.seminario.tresox.repositories.ContentRepository;
import com.seminario.tresox.repositories.ContentTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContentService {
    private ContentRepository contentRepository;
    private ContentTypeRepository contentTypeRepository;

    @Autowired
    ContentService(ContentRepository contentRepository, ContentTypeRepository contentTypeRepository) {
        this.contentRepository = contentRepository;
        this.contentTypeRepository = contentTypeRepository;
    }

    public List<Content> getAllContents() {
        return contentRepository.findAll();
    }

    public List<Content> getContentByTitle(String title) {
        return contentRepository.findContentByTitle(title);
    }

    public List<ContentType> getAllContentTypes() {
        return contentTypeRepository.findAll();
    }

    public ContentDTO updateStock(ContentDTO content) {
        System.out.println(content.getCont_id());
        System.out.println(content.getCont_stock());
        contentRepository.updateStock(content);
        return content;
    }

}
