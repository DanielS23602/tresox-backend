package com.seminario.tresox.repositories;

import com.seminario.tresox.models.ContentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ContentTypeRepository extends JpaRepository<ContentType, Long> {

}
