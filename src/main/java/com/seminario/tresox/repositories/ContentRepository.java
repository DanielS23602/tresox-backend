package com.seminario.tresox.repositories;

import com.seminario.tresox.models.Content;
import com.seminario.tresox.models.dto.ContentDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;


@Repository
public interface ContentRepository extends JpaRepository<Content, Long> {

    @Query(value = "SELECT * FROM tb_content WHERE cont_name iLIKE %:title%", nativeQuery = true)
    List<Content> findContentByTitle(@Param("title") String title);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE tb_content SET cont_stock =:#{#content.cont_stock} WHERE cont_id =:#{#content.cont_id}", nativeQuery = true)
    void updateStock(@Param("content") ContentDTO content);
}
