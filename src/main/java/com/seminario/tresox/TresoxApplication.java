package com.seminario.tresox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TresoxApplication {

	public static void main(String[] args) {
		SpringApplication.run(TresoxApplication.class, args);
	}

}
