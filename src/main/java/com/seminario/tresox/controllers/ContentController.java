package com.seminario.tresox.controllers;

import com.seminario.tresox.models.Comment;
import com.seminario.tresox.models.Content;
import com.seminario.tresox.models.ContentType;
import com.seminario.tresox.models.User;
import com.seminario.tresox.models.dto.CommentDTO;
import com.seminario.tresox.models.dto.ContentDTO;
import com.seminario.tresox.models.dto.SimpleObjectResponse;
import com.seminario.tresox.services.impl.CommentService;
import com.seminario.tresox.services.impl.ContentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@Api(tags = "Content Type API")
@RequestMapping("/content")
@CrossOrigin
@Validated
public class ContentController {
    public static final String URL_CONTROLLER = "/content";
    private final ContentService contentService;
    private final CommentService commentService;
    ModelMapper modelMapper = new ModelMapper();

    ContentController(ContentService contentService, CommentService commentService) {
        this.contentService = contentService;
        this.commentService = commentService;
    }

    @Bean
    public ModelMapper contentModelMapper() {
        return modelMapper;
    }

    @ApiOperation(nickname = "findall", notes = "Este método lista todos los contenidos.", value = "Listar Contenidos", response = Content.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Listados de contenido con éxito", response = Content.class),
            @ApiResponse(code = 400, message = "Posiblemente el token ha expirado", response = Content.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = Content.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = Content.class)})
    @GetMapping(value = "/findContentAll", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> findAll() {
        try {
            return new ResponseEntity<>(
                    SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Listados de contenido con éxito").value(contentService.getAllContents()).build(),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(
                    SimpleObjectResponse.builder().code(HttpStatus.BAD_REQUEST.value()).message("Posiblemente el token ha expirado").value(e).build(),
                    HttpStatus.BAD_REQUEST);
        }

    }

    @ApiOperation(nickname = "findcontentbytitle", notes = "Este método busca contenido a partir del titulo.", value = "Buscar contenido", response = Content.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Contenido encontrado con éxito", response = Content.class),
            @ApiResponse(code = 400, message = "Posiblemente el token ha expirado / contenido no encontrado", response = Content.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = Content.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = Content.class)})
    @GetMapping(value = "/findContentByTitle", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> findContentByTitle(@RequestBody String title) {
        try {
            return new ResponseEntity<>(
                    SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Contenido encontrado con éxito").value(contentService.getContentByTitle(title)).build(),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(
                    SimpleObjectResponse.builder().code(HttpStatus.BAD_REQUEST.value()).message("Posiblemente el token ha expirado").value(e).build(),
                    HttpStatus.BAD_REQUEST);
        }

    }

    @ApiOperation(nickname = "findallcontenttypes", notes = "Este método lista todos los tipos de contenidos.", value = "Listar tipos de Contenidos", response = ContentType.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Listados de tipos de contenido con éxito", response = ContentType.class),
            @ApiResponse(code = 400, message = "Posiblemente el token ha expirado", response = ContentType.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = ContentType.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = ContentType.class)})
    @GetMapping(value = "/findContentTypeAll", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> findAllContentTypes() {
        try {
            return new ResponseEntity<>(
                    SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Listados de tipos de contenido con éxito").value(contentService.getAllContentTypes()).build(),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(
                    SimpleObjectResponse.builder().code(HttpStatus.BAD_REQUEST.value()).message("Posiblemente el token ha expirado").value(e).build(),
                    HttpStatus.BAD_REQUEST);
        }

    }

    @ApiOperation(nickname = "savecomment", notes = "Este método guarda comentario del usuario.", value = "Guardar Comentario", response = Comment.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Comentario guardado con éxito", response = Comment.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = Comment.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = Comment.class)})
    @RequestMapping(value = "/saveComment", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
    public ResponseEntity<SimpleObjectResponse> saveComment(@NotNull(message = "Comment no puede ser nulo.") @RequestBody CommentDTO comment) {
        commentService.saveComment(comment);
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Comentario guardado con éxito").value(comment).build(),
                HttpStatus.OK);
    }

    @ApiOperation(nickname = "updateStock", notes = "Este método actualiza el stock del contenido.", value = "Actualizar stock", response = Content.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Stock actualizado con éxito", response = Content.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = Content.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = Content.class)})
    @PutMapping(value = "/updateStock", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> updateContentStock(@NotNull(message = "ContentDto no puede ser nulo.") @Valid @RequestBody ContentDTO contentDTO) {
        contentService.updateStock(contentDTO);
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Stock actualizado con éxito").value(contentDTO).build(),
                HttpStatus.OK);
    }
}
