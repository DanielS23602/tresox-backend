package com.seminario.tresox.controllers;

import com.seminario.tresox.config.security.JwtTokenUtil;
import com.seminario.tresox.models.jwt.JwtRequest;
import com.seminario.tresox.models.jwt.JwtResponse;
import com.seminario.tresox.models.dto.SimpleObjectResponse;
import com.seminario.tresox.models.User;
import com.seminario.tresox.models.dto.UserDTO;
import com.seminario.tresox.services.impl.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@Api(tags = "User Type API")
@RequestMapping("/user")
@CrossOrigin
@Validated
public class AuthController {

    public static final String URL_CONTROLLER = "/user";
    private final UserService userService;
    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    public AuthController(UserService userService) {
        this.userService = userService;
    }

    @Bean
    public ModelMapper authModelMapper() {
        return modelMapper;
    }

    @ApiOperation(nickname = "Authenticate", notes = "Este método es para autenticar al usuario.", value = "Autenticar usuario", response = User.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Usuario autenticado con éxito", response = User.class),
            @ApiResponse(code = 400, message = "Credenciales incorrectas", response = User.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = User.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = User.class)})
    @PostMapping(value = "/authenticate", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
        try {
            authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
            final UserDetails userDetails = userService
                    .loadUserByUsername(authenticationRequest.getUsername());
            final String token = jwtTokenUtil.generateToken(userDetails);
            return new ResponseEntity<>(SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Usuario autenticado con éxito").value(new JwtResponse(token)).build(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(SimpleObjectResponse.builder().code(HttpStatus.BAD_REQUEST.value()).message("Credenciales incorrectas").value(e).build(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(nickname = "Registrar", notes = "Este método registra un usuario.", value = "Registrar usuario", response = User.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Usuario registrado con éxito", response = User.class),
            @ApiResponse(code = 400, message = "El usuario ya existe", response = User.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = User.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = User.class)})
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<SimpleObjectResponse> saveUser(@RequestBody UserDTO user) {
        try {
            var res = userService.save(user);
            if (res != null) {
                return new ResponseEntity<>(
                        SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Usuario registrado con éxito").value(user).build(),
                        HttpStatus.OK);
            } else {
                return new ResponseEntity<>(
                        SimpleObjectResponse.builder().code(HttpStatus.BAD_REQUEST.value()).message("El usuario ya existe").value(user).build(),
                        HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(SimpleObjectResponse.builder().code(HttpStatus.BAD_REQUEST.value()).message("Error al registrar").value(e).build(), HttpStatus.BAD_REQUEST);
        }

    }


    @ApiOperation(nickname = "update", notes = "Este método actualiza un usuario.", value = "Actualizar usuario", response = User.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Usuario actualizado con éxito", response = User.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = User.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = User.class)})
    @PutMapping(value = "/update", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> update(@NotNull(message = "UserDto no puede ser nulo.") @Valid @RequestBody User user) {
        userService.update(modelMapper.map(user, User.class));
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Usuario actualizado con éxito").build(),
                HttpStatus.OK);
    }


    @ApiOperation(nickname = "findall", notes = "Este método lista todos los usuarios.", value = "Listar Usuarios", response = User.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Listados de usuarios con éxito", response = User.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = User.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = User.class)})
    @GetMapping(value = "/findUserAll", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> findAll() {
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Listados de usuarios con éxito").value(userService.getAllUsers()).build(),
                HttpStatus.OK);
    }

    @ApiOperation(nickname = "findbyid", notes = "Este método lista al usuario mediante la búsqueda con su id.", value = "Buscar Usuario", response = User.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Encontrado con éxito", response = User.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = User.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = User.class)})
    @GetMapping(value = "/findById/{id}", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> findById(@PathVariable Long id) {
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Encontrado con éxito").value(userService.findById(id)).build(),
                HttpStatus.OK);
    }

    @ApiOperation(nickname = "deletebyid", notes = "Este método elimina al usuario mediante su id.", value = "Eliminar Usuario", response = User.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Eliminado con éxito", response = User.class),
            @ApiResponse(code = 404, message = "No se encontró el recurso solicitado", response = User.class),
            @ApiResponse(code = 500, message = "Error con la conexión a la base de datos", response = User.class)})
    @DeleteMapping(value = "/deleteById/{id}", produces = "application/json;charset=UTF-8")
    public ResponseEntity<SimpleObjectResponse> deleteById(@PathVariable Long id) {
        userService.deleteById(id);
        return new ResponseEntity<>(
                SimpleObjectResponse.builder().code(HttpStatus.OK.value()).message("Eliminado con éxito").value(null).build(),
                HttpStatus.OK);
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }


}